package org.feizal;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Tag extends PanacheEntity {
    @Column(length = 50)
    public String label;

    public Long posts;
}
