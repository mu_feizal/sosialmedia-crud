package org.feizal;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Post extends PanacheEntity {

    @Column(length = 50)
    public String title;

    @Column(length = 255)
    public String content;
}
