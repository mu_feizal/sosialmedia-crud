package org.feizal;

import org.jboss.logging.annotations.Pos;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/sosialmedia")
public class SosialMediaResource {

    @GET
    @Path("/post")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPost() {
        List<Post> posts = Post.listAll();
        return Response.ok(posts).build();
    }

    @GET
    @Path("/post/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPostById(@PathParam("id") Long id) {
        return Post.findByIdOptional(id)
                .map(post -> Response.ok(post).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @GET
    @Path("/post/title/{title}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPostByTitle(@PathParam("title") String title) {
        return Post.find("title", title)
                .singleResultOptional()
                .map(post -> Response.ok(post).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }       

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/post")
    public Response createPost(Post post) {
        Post.persist(post);
        if (post.isPersistent()) {
            return Response.created(URI.create("/post" + post.id)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/post/{id}")
    public Response deletePostById(@PathParam("id") Long id) {
        boolean deleted = Post.deleteById(id);
        if (deleted) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/post/{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePostById(@PathParam("id") Long id, Post post) {
        Post entity = Post.findById(id);
        if (entity == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        entity.title = post.title;
        entity.content = post.content;
        return Response.ok(entity).build();
    }

    //TAGS

    @GET
    @Path("/tag")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTag() {
        List<Tag> tags = Tag.listAll();
        return Response.ok(tags).build();
    }

    @GET
    @Path("/tag/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTagById(@PathParam("id") Long id) {
        return Tag.findByIdOptional(id)
                .map(tag -> Response.ok(tag).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @GET
    @Path("/tag/label/{label}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTagByLabel(@PathParam("label") String label) {
        return Tag.find("label", label)
                .singleResultOptional()
                .map(tag -> Response.ok(tag).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/tag")
    public Response createTag(Tag tag) {
        Tag.persist(tag);
        if (tag.isPersistent()) {
            return Response.created(URI.create("/tag" + tag.id)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @DELETE
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/tag/{id}")
    public Response deleteTagById(@PathParam("id") Long id) {
        boolean deleted = Tag.deleteById(id);
        if (deleted) {
            return Response.noContent().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/tag/{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTagById(@PathParam("id") Long id, Tag tag) {
        Tag entity = Tag.findById(id);
        if (entity == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        entity.label = tag.label;
        entity.posts = tag.posts;
        return Response.ok(entity).build();
    }

}